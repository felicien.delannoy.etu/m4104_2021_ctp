package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.core.view.GestureDetectorCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.util.ArrayList;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel modele;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        this.DISTANCIEL=getResources().getStringArray(R.array.list_salles)[0];
        System.out.println(DISTANCIEL);
        this.poste="";
        this.salle=this.DISTANCIEL;
        // TODO Q2.c
        modele=new SuiviViewModel(getActivity().getApplication());
        // TODO Q4
        Spinner spinnerSalle = view.findViewById(R.id.spSalle);
        String[] listeDeSalle = getResources().getStringArray(R.array.list_salles);
        spinnerSalle.setAdapter(new ArrayAdapter<String>( getContext(),android.R.layout.simple_list_item_1,listeDeSalle));
        Spinner spinnerPoste = view.findViewById(R.id.spPoste);
        String[] listeDePoste = getResources().getStringArray(R.array.list_postes);
        spinnerPoste.setAdapter(new ArrayAdapter<String>( getContext(),android.R.layout.simple_list_item_1,listeDePoste));

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText logintv = view.findViewById(R.id.tvLogin);
            modele.setUsername(logintv.getText()+"");
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });


        // TODO Q5.b
        update();
       spinnerSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                   @Override
                                                   public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                       update();
                                                   }

                                                   @Override
                                                   public void onNothingSelected(AdapterView<?> parent) {
                                                       update();
                                                           }
                                               });

                // TODO Q9
    }

    // TODO Q5.a
    void update(){
        Spinner spinnerSalle =(Spinner) getActivity().findViewById(R.id.spSalle);
        Spinner spinnerPoste =(Spinner) getActivity().findViewById(R.id.spPoste);
       // String laSalle=  spinnerSalle.

        int laSalle = spinnerSalle.getSelectedItemPosition();
        String lePoste = spinnerPoste.getSelectedItem().toString();

        if(laSalle==0){
            spinnerPoste.setVisibility(View.GONE);
            spinnerPoste.setEnabled(false);
            this.modele.setLocalisation(spinnerSalle.getSelectedItem().toString());
        }
        else{
            spinnerPoste.setVisibility(View.VISIBLE);
            spinnerPoste.setEnabled(true);
            this.modele.setLocalisation(spinnerSalle.getSelectedItem().toString()+":"+lePoste);
        }
    }


    // TODO Q9
}