package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    // TODO Q2.a
    private Integer nextQuestion= 0;
    private String[] questions ;
    private MutableLiveData<Integer> liveNextQuestions;

    public SuiviViewModel(Application application) {
        super(application);
        iniQuestions(getApplication().getApplicationContext());
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        liveNextQuestions= new MutableLiveData<>();
    }

    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    
    // TODO Q2.a
    void iniQuestions(Context context){
        this.questions= context.getResources().getStringArray(R.array.list_questions);
    }

   public String getQuestions (int position){
        return this.questions[position];
    }
    public  LiveData<Integer> getLiveNextQuestions(){
        return this.liveNextQuestions;
    }
    public void setNextQuestion(Integer nextQuestion){
        this.liveNextQuestions.setValue(nextQuestion);
    }
    public Integer getNextQuestion(){
        return this.liveNextQuestions.getValue();
    }


}
